using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Skelet
{
    [Serializable]
    public class SpawnWave
    {
        [SerializeField]
        private EnemyAmount[] enemies;

        public EnemyAmount[] Enemies { get { return this.enemies; } }
    }
}