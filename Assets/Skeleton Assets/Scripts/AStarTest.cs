﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using URandom = UnityEngine.Random;

public class AStarTest : MonoBehaviour
{
    [SerializeField]
    private int width = 5;

    [SerializeField]
    private int height = 5;

    [SerializeField]
    private float scale = 1f;

    [SerializeField]
    private bool allowDiagonal = true;

    [SerializeField]
    private Vector2 offset;

    [SerializeField]
    private int impassableSeeds = 0;

    [SerializeField]
    private float seedSizeMin = 2f;

    [SerializeField]
    private float seedSizeMax = 4f;

    [SerializeField]
    private Vector2 from = new Vector2(0, 0);

    [SerializeField]
    private Vector2 to = new Vector2(0, 0);

    [SerializeField]
    private bool benchMark;

    [SerializeField]
    private bool rebuild;

    private AStar aStar;
    private Plane plane = new Plane(Vector3.back, 0f);
    private int lastUsedSeedCount;

    private List<Vector2> latestPath;

    private void Update()
    {
        if (this.rebuild || this.aStar == null || this.aStar.GridWidth != this.width || this.aStar.GridHeight != this.height || this.lastUsedSeedCount != this.impassableSeeds)
        {
            this.aStar = this.GenerateNewGrid();
        }

        this.aStar.GridScale = this.scale;
        this.aStar.AllowDiagonal = this.allowDiagonal;
        this.aStar.GridOffset = this.offset;

        //if (Input.GetMouseButton(0))
        //{
        //    this.from = this.GetMousePositionOnPlane();
        //}
        //else if (Input.GetMouseButton(1))
        //{
        this.to = this.GetMousePositionOnPlane();
        //}

        try
        {
            this.latestPath = this.aStar.GetPath(this.from, this.to, null) ?? this.latestPath;
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }

        if (this.benchMark)
        {
            this.benchMark = false;

            int count = 100;
            var watch = System.Diagnostics.Stopwatch.StartNew();

            for (int i = 0; i < count; i++)
            {
                Vector2 goal = new Vector2(URandom.Range(0, this.width - 1f), URandom.Range(0, this.height - 1f));
                this.aStar.GetPath(this.from, goal, null);
            }

            watch.Stop();

            Debug.Log("Average time per path: " + (double)watch.ElapsedMilliseconds / count);
        }
    }

    private void OnDrawGizmos()
    {
        if (this.aStar == null || this.aStar.GridWidth != this.width || this.aStar.GridHeight != this.height)
        {
            this.aStar = this.GenerateNewGrid();
        }

        if (this.aStar != null)
        {
            var grid = this.aStar.Grid;

            for (int x = 0; x < this.aStar.GridWidth; x++)
            {
                for (int y = 0; y < this.aStar.GridHeight; y++)
                {
                    if (grid[x, y] == null || grid[x, y].IsPassable(null))
                    {
                        Gizmos.color = Color.blue;
                    }
                    else
                    {
                        Gizmos.color = Color.red;
                    }

                    Gizmos.DrawCube(new Vector3(x * this.scale + this.offset.x, y * this.scale + this.offset.y), new Vector3(1, 1, 0.1f) * this.scale);
                }
            }
        }

        if (this.latestPath != null)
        {
            Gizmos.color = Color.green;

            var count = this.latestPath.Count - 1;

            for (int i = 0; i < count; i++)
            {
                Gizmos.DrawLine((Vector3)this.latestPath[i] + Vector3.back * 0.1f, (Vector3)this.latestPath[i + 1] + Vector3.back * 0.1f);
            }
        }

        this.Update();
    }

    private Vector2 GetMousePositionOnPlane()
    {
        float enter;
        Ray ray = Camera.current.ScreenPointToRay(new Vector3(Screen.width, Screen.height) * 0.5f);

        if (this.plane.Raycast(ray, out enter))
        {
            return ray.origin + ray.direction * enter;
        }

        return Vector2.zero;
    }

    private AStar GenerateNewGrid()
    {
        this.rebuild = false;
        var grid = new IPathNode[this.width, this.height];

        this.lastUsedSeedCount = this.impassableSeeds;

        for (int i = 0; i < this.impassableSeeds; i++)
        {
            var seed = new Vector2(
                Mathf.RoundToInt(URandom.Range(0f, this.width - 1f)),
                Mathf.RoundToInt(URandom.Range(0f, this.height - 1f))
            );

            float size = URandom.Range(this.seedSizeMin, this.seedSizeMax);

            for (int x = 0; x < this.width; x++)
            {
                for (int y = 0; y < this.height; y++)
                {
                    if (grid[x, y] != null)
                    {
                        continue;
                    }

                    var point = new Vector2(x, y);
                    if (Vector2.Distance(seed, point) <= size)
                    {
                        grid[x, y] = new ImpassableNode();
                    }
                }
            }
        }

        return new AStar(grid);
    }
}

public class ImpassableNode : IPathNode
{
    public bool IsPassable(string key)
    {
        return false;
    }
}