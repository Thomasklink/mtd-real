using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Skelet
{
    public abstract class Tower : Unit
    {
        [SerializeField]
        private float cost = 100f;

        [SerializeField]
        private float cooldown = 1f;

        [SerializeField]
        private float range = 10f;

        private float nextFire = 0f;

        protected virtual void Update()
        {
            Enemy enemy = this.FindTargetEnemy();

            if (enemy != null)
            {
                this.LookAtEnemy(enemy);

                if (Time.time > this.nextFire)
                {
                    this.Shoot(enemy);
                    this.nextFire = Time.time + this.cooldown;
                }
            }
        }

        protected virtual void LookAtEnemy(Enemy enemy)
        {
            throw new NotImplementedException();
        }

        protected virtual Enemy FindTargetEnemy()
        {
            throw new NotImplementedException();
        }

        public override bool IsPassable(string key)
        {
            return false;
        }

        protected abstract void Shoot(Enemy enemy);
    }
}