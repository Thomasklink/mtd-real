using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Skelet
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private int width = 21;

        [SerializeField]
        private int height = 21;

        [SerializeField]
        private int lives = 20;

        [SerializeField]
        private Vector2 from = new Vector2(10, 20);

        [SerializeField]
        private Vector2 to = new Vector2(10, 0);

        [SerializeField]
        private SpawnWave[] waves;

        [SerializeField]
        private float spawnInterval = 0.5f;

        private Queue<SpawnInfo> enemiesToSpawn = new Queue<SpawnInfo>();
        private GameState state;
        private Unit[,] grid;
        private List<Enemy> enemies = new List<Enemy>();
        private AStar aStar;
        private float nextSpawn;

        public int CurrentWave { get; private set; }

        public GameState State { get { return this.state; } }

        public Unit[,] Grid { get { return this.grid; } }

        public List<Enemy> Enemies { get { return this.enemies; } }

        public static GameManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;

                this.grid = new Unit[this.width, this.height];

                // Populate grid

                this.aStar = new AStar(this.grid);
                this.state = GameState.Building;

                var camera = Camera.main;

                camera.transform.position = new Vector3((this.width - 1) * 0.5f, (this.height - 1) * 0.5f, -10);
                camera.orthographicSize = Mathf.Max(this.width, this.height) * 1.1f;
            }
        }

        private void OnDrawGizmos()
        {
            if (this.aStar != null)
            {
                var grid = this.aStar.Grid;

                for (int x = 0; x < this.aStar.GridWidth; x++)
                {
                    for (int y = 0; y < this.aStar.GridHeight; y++)
                    {
                        if (grid[x, y] == null || grid[x, y].IsPassable(null))
                        {
                            Gizmos.color = Color.blue * 0.5f;
                        }
                        else
                        {
                            Gizmos.color = Color.red * 0.5f;
                        }

                        Gizmos.DrawCube(new Vector3(x /** this.scale + this.offset.x*/, y /** this.scale + this.offset.y*/, 1), new Vector3(1, 1, 0.1f)/* * this.scale*/);
                    }
                }
            }
        }

        private void OnGUI()
        {
            GUILayout.Box(this.state.ToString());
        }

        private void Update()
        {
            switch (this.state)
            {
                case GameState.Building:

                    // Let player build

                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        this.state = GameState.Spawning;

                        SpawnWave wave = this.waves[this.CurrentWave];
                        Dictionary<string, List<Vector2>> paths = new Dictionary<string, List<Vector2>>();

                        foreach (var enemyType in wave.Enemies)
                        {
                            List<Vector2> path;

                            if (paths.ContainsKey(enemyType.Enemy.Key))
                            {
                                path = paths[enemyType.Enemy.Key];
                            }
                            else
                            {
                                path = this.aStar.GetPath(this.from, this.to, enemyType.Enemy.Key);
                                paths.Add(enemyType.Enemy.Key, path);
                            }

                            for (int i = 0; i < enemyType.Amount; i++)
                            {
                                this.enemiesToSpawn.Enqueue(new SpawnInfo(enemyType.Enemy, path));
                            }
                        }
                    }

                    break;

                case GameState.Spawning:

                    // spawn waves, do whatever

                    if (Time.time >= this.nextSpawn)
                    {
                        var next = this.enemiesToSpawn.Dequeue();
                        this.SpawnEnemy(next.Enemy, next.Path);

                        this.nextSpawn = Time.time + this.spawnInterval;
                    }

                    if (this.enemiesToSpawn.Count == 0)
                    {
                        this.state = GameState.WaveIncoming;
                    }

                    break;

                case GameState.WaveIncoming:

                    // remove destroyed enemies
                    for (int i = 0; i < this.enemies.Count; i++)
                    {
                        if (this.enemies[i] == null)
                        {
                            this.enemies.RemoveAt(i);
                        }
                    }

                    // wait for all enemies to be dead
                    if (this.enemies.Count == 0)
                    {
                        // wave complete!

                        this.CurrentWave++;

                        if (this.CurrentWave >= this.waves.Length)
                        {
                            // Win somehow
                            Debug.Log("You're winner!");
                        }
                        else
                        {
                            this.state = GameState.Building;
                        }
                    }

                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        protected void SpawnEnemy(Enemy enemy, List<Vector2> path)
        {
            var spawned = GameObject.Instantiate(enemy.gameObject).GetComponent<Enemy>();
            spawned.transform.position = path[0];
            this.enemies.Add(spawned);
            spawned.SetPath(path);
        }

        public void OnEnemyReachedGoal(Enemy enemy)
        {
            this.lives--;

            if (this.lives <= 0)
            {
                // Somehow lose
                Debug.Log("You're dead!");
            }
        }

        private struct SpawnInfo
        {
            public readonly Enemy Enemy;
            public readonly List<Vector2> Path;

            public SpawnInfo(Enemy enemy, List<Vector2> path)
            {
                this.Enemy = enemy;
                this.Path = path;
            }
        }
    }
}